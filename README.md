# Ignacy's OSX Dotfiles
My OSX configuration files for:
- Yabai (Window Manager) 
- Skhd (Hotkey Daemon)
- Karabiner Elements (Keyboard Remap Tool)
- Spicetify (Tool for customizing spotify)
- Zsh (Shell)
- Neovim (Editor)

Screenshot of my Desktop:

![Preview](https://github.com/IgnacyWie/Dotfiles/blob/master/preview/Screenshot%202020-07-15%20at%2006.43.56.png)
