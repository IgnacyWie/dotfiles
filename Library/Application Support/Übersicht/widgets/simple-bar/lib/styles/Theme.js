// uncomment this, and comment the rest of the lines to use pywal colors.
// export { Theme } from './ThemePywal.js'

export const Theme = {
  main: '#040A13',
  minor: '#3D86AD',
  accent: '#FFD484',
  red: '#3D86AD',
  green: '#3D86AD',
  yellow: '#3D86AD',
  blue: '#3D86AD',
  magenta: '#3D86AD',
  cyan: '#3D86AD',
  font: 'JetBrains Mono, monospace',
  easing: 'cubic-bezier(0.4, 0, 0.2, 1)'
};
