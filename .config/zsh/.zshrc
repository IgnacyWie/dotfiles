#           _
#   _______| |__  _ __ ___
#  |_  / __| '_ \| '__/ __|
#   / /\__ \ | | | | | (__
#  /___|___/_| |_|_|  \___|
#

# Appearance---------------------------------------------------------------------------------------------------------------------------------------------------------------
ZSH_THEME="typewritten"
export TYPEWRITTEN_PROMPT_LAYOUT="singleline"
export TYPEWRITTEN_SYMBOL=">"
export TYPEWRITTEN_CURSOR="block"

# Plugins-------------------------------------------------------------------------------------------------------------------------------------------------------------------
plugins=(
	zsh-syntax-highlighting
	osx
	z
	colored-man-pages
	sudo
	vi-mode
)



# Startup Programs---------------------------------------------------------------------------------------------------------------------------------------------------------
neofetch

# Aliases------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Dotfiles
alias skhdconf='nvim ~/.config/skhd/skhdrc'
alias yabaiconf='nvim ~/.config/yabai/yabairc'
alias spaceconf='nvim ~/.config/spacebar/spacebarrc'
alias zshconf='nvim ~/.config/zsh/.zshrc'
alias config='/usr/bin/git --git-dir=$HOME/.scripts/dotfiles/ --work-tree=/Users/ignacywielogorski'
alias kittyconf='nvim ~/.config/kitty/kitty.conf'
alias zshenv='nvim ~/.zshenv'
alias nvimconf='nvim ~/.config/nvim/init.vim'
alias keyconf='nvim ~/.config/karabiner.edn'
alias vpnconf='ssh 35.232.242.33'
alias kittydir='cd ~/.config/kitty'
alias vifmconf='nvim ~/.config/vifm/vifmrc'
alias muttconf='nvim ~/.config/mutt/muttrc'
alias rangerconf='nvim ~/.config/ranger/rc.conf'

# Typos and Renames
alias sl='ls'
alias brwe='brew'
alias clera='clear'
alias show-ssh='pbcopy < ~/.ssh/id_rsa.pub'
alias vim='nvim'
alias brw='brew'
alias v='nvim'
alias mutt='neomutt'
alias ls='ls -a'

# Scripts
alias dotupdate='bash ~/.scripts/dotupdate.sh'
alias connecthome='zsh ~/.scripts/homewifi.sh'

# Other
alias icat="kitty +kitten icat $1"
alias se="du -a ~/.scripts/ ~/.config/ | awk '{print $2}' | fzf | xargs $EDITOR"
alias yt="youtube-dl -f bestvideo+bestaudio"
alias aliasearch="alias | fzf"
alias keybr="open 'https://keybr.com'"
alias f='ranger'

# Other---------------------------------------------------------------------------------------------------------------------------------------------------------------------

if [ -f '/Users/ignacywielogorski/Downloads/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/ignacywielogorski/Downloads/google-cloud-sdk/path.zsh.inc'; fi
if [ -f '/Users/ignacywielogorski/Downloads/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/ignacywielogorski/Downloads/google-cloud-sdk/completion.zsh.inc'; fi
export PATH="/usr/local/sbin:$PATH"
source /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh
source $ZSH/oh-my-zsh.sh
autoload -Uz compinit
compinit

# Completion for kitty
kitty + complete setup zsh | source /dev/stdin
