"        _                    
" __   _(_)_ __ ___  _ __ ___ 
" \ \ / / | '_ ` _ \| '__/ __|
"  \ V /| | | | | | | | | (__ 
"   \_/ |_|_| |_| |_|_|  \___|

" Plugins --------------------------------------------------------------------------
call plug#begin('~/.config/nvim/bundle')

Plug 'morhetz/gruvbox'
Plug 'dracula/vim'
Plug 'ThePrimeagen/vim-be-good', {'do': './install.sh'}
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/goyo.vim'
Plug 'vifm/vifm.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'ap/vim-css-color'
Plug 'tpope/vim-surround'
Plug 'itchyny/lightline.vim'
Plug 'terryma/vim-multiple-cursors'
Plug 'tc50cal/vim-terminal'

call plug#end()

" Colorscheme ----------------------------------------------------------------------
colorscheme gruvbox
set background=dark

" General Settings ------------------------------------------------------------------
syntax on  
filetype plugin indent on
set hlsearch
set nocompatible
set ignorecase
set nu
set noerrorbells
set visualbell
set nowrap
set smartcase
set noswapfile
set incsearch
set smartindent
set ruler
set showcmd
set relativenumber
set clipboard=unnamedplus
set encoding=UTF-8
set cursorline
set history=100
set title
set hidden

" Keyboard remaps ------------------------------------------------------------------
let mapleader = ' '
nnoremap S :%s//g<Left><Left>
nmap <leader>w :w!<cr>
nmap <leader>q :q<cr>
nmap <leader>r :source ~/.config/nvim/init.vim<cr>
nmap <leader>c :FZF ~/.config<cr>
nmap <leader>f :FZF<space>
nmap <leader>F :FZF<cr>
nmap <leader>p :wincmd v<bar> :Ex <bar> :vertical resize 30<X=CR><cr>
nmap <leader>h :wincmd h<cr>
nmap <leader>j :wincmd j<cr>
nmap <leader>k :wincmd k<cr>
nmap <leader>l :wincmd l<cr>
nmap <leader>v :Vifm<cr>
nmap <leader>d :colorscheme dracula<cr>
nmap <leader>s :colorscheme gruvbox<cr>
nmap <leader>g :Goyo<cr>
nmap <leader>t :TerminalVSplit zsh<cr>
